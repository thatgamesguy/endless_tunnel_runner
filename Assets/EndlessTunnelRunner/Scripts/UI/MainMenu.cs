﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
	public LevelSelectMenu LightMenu;
	public LevelSelectMenu DarkMenu;
	
	void Awake ()
	{
		Application.targetFrameRate = 1000;
		LevelCompletion.instance.Load ();
	}
	
	public void EnableLightMenu ()
	{
		LightMenu.gameObject.SetActive (true);
		LightMenu.Initialise ();
		gameObject.SetActive (false);
	}
	
	public void EnableDarkMenu ()
	{
		DarkMenu.gameObject.SetActive (true);
		DarkMenu.Initialise ();
		gameObject.SetActive (false);
	}
	

	
/*	public void StartGame (int numOfPlayers)
	{
		
		if (numOfPlayers == 2) {
			playerTwo.StartGame ();
		}
		
		
		Cursor.visible = false;
		AudioPlayer.instance.Play ();
		gameObject.SetActive (false);
	}*/
	

	

}


