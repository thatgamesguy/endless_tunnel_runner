﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class LevelSelectMenu : MonoBehaviour
{
	public Player player;
	public MainMenu mainMenu;
	public Camera UICamera;
	public Color cameraColour;
	public Material ObstacleMaterial;
	public Material PipeMaterial;
	public Button[] LevelButtons;

	public Sprite CompleteLevelImage;
	public Sprite DefaultImage;

	public enum SelectScreenType
	{
		LIGHT,
		DARK
	}
	;
	public SelectScreenType LevelType;


	private Color initialColour;

	private int currentIndex;
	
	public void Initialise ()
	{
		initialColour = UICamera.backgroundColor;
		UICamera.backgroundColor = cameraColour;

		for (int i = 0; i < LevelButtons.Length; i++) {
			if (IsLevelComplete (i)) {
				LevelButtons [i].GetComponent<Image> ().overrideSprite = CompleteLevelImage;
			} else {
				LevelButtons [i].GetComponent<Image> ().overrideSprite = DefaultImage;
			}

		}
	}

	private bool IsLevelComplete (int index)
	{
		if (LevelType == SelectScreenType.LIGHT) {
			return LevelCompletion.instance.CompleteLight [index];
		} else {
		}
		return LevelCompletion.instance.CompleteDark [index];
	
	}

	public void StartGame (LevelData data)
	{
		currentIndex = data.Index;

		UICamera.enabled = false;
		
		LevelMaterial.instance.CurrentObstacleMaterial = ObstacleMaterial;
		LevelMaterial.instance.CurrentPipeMaterial = PipeMaterial;
		
		player.SetMenu (this);
		player.StartGame ();
		
		
		gameObject.SetActive (false);
		Cursor.visible = false;
		AudioPlayer.instance.SetClip (data.LevelAudio);
		HUD.instance.Initialise ();
		AudioPlayer.instance.Play ();
	}
	
	public void EndGame (bool beatLevel)
	{
		if (beatLevel) {
			UpdateData (beatLevel);
		}

		gameObject.SetActive (true);
		Cursor.visible = true;
		AudioPlayer.instance.Stop ();
		HUD.instance.LevelEnd ();
		UICamera.enabled = true;
	}

	private void UpdateData (bool beatLevel)
	{
		if (LevelType == SelectScreenType.LIGHT) {
			LevelCompletion.instance.CompleteLight [currentIndex] = beatLevel;
		} else {
			LevelCompletion.instance.CompleteDark [currentIndex] = beatLevel;
		}
	}
	
	public void TransitionToMainMenu ()
	{
		ResetCameraBackground ();
		mainMenu.gameObject.SetActive (true);
		gameObject.SetActive (false);
	}
	
	private void ResetCameraBackground ()
	{
		UICamera.backgroundColor = initialColour;
	}
}
