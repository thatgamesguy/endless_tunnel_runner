﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour
{
	public GameObject pauseMenuBackground;
	public Player player;

	public void BackToGame ()
	{
		Time.timeScale = 1f;
		AudioPlayer.instance.SetPitch (1f);
		DisablePauseMenu ();
	}

	public void TransitionToLevelSelectMenu ()
	{
		player.EndGame (false);
		DisablePauseMenu ();
	}

	public void RestartLevel ()
	{

	}

	private void DisablePauseMenu ()
	{
		pauseMenuBackground.SetActive (false);
		gameObject.SetActive (false);
	}
}
