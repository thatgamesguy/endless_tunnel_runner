﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class AudioPlayer : MonoBehaviour
{
	private AudioSource _audio;

	private int qSamples = 1024;  // array size
	//private float refValue = 0.1f; // RMS value for 0 dB
	private float[] samples;
	//private float rmsValue;   // sound level - RMS
	//var dbValue: float;    // sound level - dB
	//var volume: float = 2; // set how much the scale will vary

	private float initialPitch;

	private static AudioPlayer _instance;
	public static AudioPlayer instance { get { return _instance; } }

	void Awake ()
	{
		_instance = this;

		_audio = GetComponent<AudioSource> ();
		_audio.playOnAwake = false;
		_audio.spatialBlend = 0f;
		_audio.loop = true;

		initialPitch = _audio.pitch;

		samples = new float[qSamples];
	}
	
	public void SetClip (AudioClip clip)
	{
		_audio.clip = clip;
	}

	public void Play ()
	{
		_audio.Play ();
	}
	
	public void Stop ()
	{
		_audio.Stop ();
	}
	
	public float SongLegthSeconds ()
	{
		return _audio.clip.length;
	}

	public float GetVolume ()
	{
		if (!_audio.isPlaying)
			return 0;
	
		_audio.GetOutputData (samples, 0); // fill array with samples
		int i;
		float sum = 0;
		for (i=0; i < qSamples; i++) {
			sum += samples [i] * samples [i]; // sum squared samples
		}
		var rmsValue = Mathf.Sqrt (sum / qSamples); // rms = square root of average
		/*var dbValue = 20 * Mathf.Log10 (rmsValue / refValue); // calculate dB
		if (dbValue < -160)
			dbValue = -160; // clamp it to -160dB min*/

		return rmsValue;
	}

	public void SetPitch (float pitch)
	{
		_audio.pitch = pitch;

	}
	
	public void SetPitchForSeconds (float pitch, float seconds)
	{
		_audio.pitch = pitch;
		
		Invoke ("ResetPitch", seconds);
	}
	
	private void ResetPitch ()
	{
		_audio.pitch = initialPitch;
	}

}
