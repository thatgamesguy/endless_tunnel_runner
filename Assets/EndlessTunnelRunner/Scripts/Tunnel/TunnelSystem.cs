﻿using UnityEngine;
using System.Collections;

public class TunnelSystem : MonoBehaviour
{

	public Tunnel tunnelPrefab;
	public int numberOfInitialEmptyTunnels;
	public int tunnelCount;
	
	private Tunnel[] tunnels;
	
	private void Awake ()
	{
		tunnels = new Tunnel[tunnelCount];
		for (int i = 0; i < tunnels.Length; i++) {
			var tunnel = tunnels [i] = Instantiate<Tunnel> (tunnelPrefab);
			tunnel.transform.SetParent (transform, false);	
		}
	}
	
	public void RemoveObstacles ()
	{
		foreach (var t in tunnels) {
			t.Obstacles.RemoveCurrentObstacles ();
		}
	}
	
	private void SetPipeMaterial ()
	{
		foreach (var t in tunnels) {
			var renderer = t.GetComponent<MeshRenderer> ();
			renderer.material = LevelMaterial.instance.CurrentPipeMaterial;
		}
	}
	
	public Tunnel SetupFirstPipe ()
	{
		SetPipeMaterial ();
	
		for (int i = 0; i < tunnels.Length; i++) {
			var tunnel = tunnels [i];
			tunnel.Generate (i > numberOfInitialEmptyTunnels);
			if (i > 0) {
				tunnel.AlignWith (tunnels [i - 1]);
			}
		}
		AlignNextPipeWithOrigin ();
		transform.localPosition = new Vector3 (0f, -tunnels [1].CurveRadius);
		
		
		return tunnels [1];
	}
	
	public Tunnel SetupNextPipe ()
	{
		ShiftPipes ();
		AlignNextPipeWithOrigin ();
		tunnels [tunnels.Length - 1].Generate ();
		tunnels [tunnels.Length - 1].AlignWith (tunnels [tunnels.Length - 2]);
		transform.localPosition = new Vector3 (0f, -tunnels [1].CurveRadius);
		return tunnels [1];
	}
	
	private void ShiftPipes ()
	{
		var temp = tunnels [0];
		for (int i = 1; i < tunnels.Length; i++) {
			tunnels [i - 1] = tunnels [i];
		}
		tunnels [tunnels.Length - 1] = temp;
	}
	
	private void AlignNextPipeWithOrigin ()
	{
		Transform transformToAlign = tunnels [1].transform;
		for (int i = 0; i < tunnels.Length; i++) {
			if (i != 1)
				tunnels [i].transform.SetParent (transformToAlign);
		}
		
		transformToAlign.localPosition = Vector3.zero;
		transformToAlign.localRotation = Quaternion.identity;
		
		for (int i = 1; i < tunnels.Length; i++) {
			tunnels [i].transform.SetParent (transform);
		}
	}

}
