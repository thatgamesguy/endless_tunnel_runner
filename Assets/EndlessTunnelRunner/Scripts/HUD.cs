﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
	public Text timeLabel;
	public PauseMenu pauseMenu;
	public GameObject pauseMenuBackground;

	
	private static HUD _instance;
	public static HUD instance { get { return _instance; } }
	
	private float time;

	void Awake ()
	{
		_instance = this;
		timeLabel.enabled = false;
	}
	
	public void Initialise ()
	{
		time = AudioPlayer.instance.SongLegthSeconds ();
		timeLabel.enabled = true;
	}
	
	public void LevelEnd ()
	{
		time = 0f;
		timeLabel.enabled = false;
	}

	void Update ()
	{
		if (time <= 0) {
			UpdateTimeLabel (0.00f);
			return;
		}
		
		var timeLeft = time - Time.time;
		UpdateTimeLabel (timeLeft);
	}
	
	public void UpdateTimeLabel (float timeLeft)
	{
		var roundedRestSeconds = Mathf.CeilToInt (timeLeft);
		var displaySeconds = roundedRestSeconds % 60;
		var displayMinutes = roundedRestSeconds / 60; 
		var text = string.Format ("{0:00}:{1:00}", displayMinutes, displaySeconds);
		
		timeLabel.text = text;
	}

	public void Pause ()
	{
		Time.timeScale = 0f;
		AudioPlayer.instance.SetPitch (0f);
		pauseMenuBackground.SetActive (true);
		pauseMenu.gameObject.SetActive (true);
	}
	

}
