﻿using UnityEngine;
using System.Collections;

public class FlashingObstacle : Obstacle
{

	public override void Update ()
	{
		if (obstacle.gameObject.activeSelf) {
			obstacle.gameObject.SetActive (AudioPlayer.instance.GetVolume () > 0.08f);
		} else {
			obstacle.gameObject.SetActive (AudioPlayer.instance.GetVolume () > 0.15f);
		}
	}



}
