﻿using UnityEngine;
using System.Collections;

public class RotatingObstacle : Obstacle
{
	public float RotationSpeed = 10f;
	
	public enum RotateMode
	{
		LEFT,
		RIGHT,
		RANDOM
	}
	public RotateMode RotationDirection;

	private float rotationInput, avatarRotation;

	public override void Awake ()
	{
		base.Awake ();
		
		switch (RotationDirection) {
		case RotateMode.LEFT:
			rotationInput = -1;
			break;
		case RotateMode.RIGHT:
			rotationInput = 1;
			break;
		case RotateMode.RANDOM:
			rotationInput = (Random.value > 0.5) ? -1 : 1;
			break;
		}
		
		
	}

	public override void Update ()
	{
		base.Update ();

		avatarRotation += RotationSpeed * Time.deltaTime * rotationInput * AudioPlayer.instance.GetVolume ();

		if (avatarRotation < 0f) {
			avatarRotation += 360f;
		} else if (avatarRotation >= 360f) {
			avatarRotation -= 360f;
		}

		rotater.localRotation = Quaternion.Euler (avatarRotation, 0f, 0f);
	}

}
