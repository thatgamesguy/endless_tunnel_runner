using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpiralObstaclePlacer : ObstaclePlacer
{

	public override List<float> GenerateObstacles (Tunnel tunnel)
	{		
		var rotations = new List<float> ();
		float start = (Random.Range (0, tunnel.pipeSegmentCount) + 0.5f);
		float direction = Random.value < 0.5f ? 1f : -1f;
		
		float angleStep = tunnel.CurveAngle / tunnel.CurveSegmentCount;
		for (int i = 0; i < tunnel.CurveSegmentCount; i++) {
/*			if (Random.value < AudioPlayer.instance.GetVolume ()) {
				continue;
			}*/
			
			var item = Instantiate<Obstacle> (
				ObstaclePrefabs [Random.Range (0, ObstaclePrefabs.Length)]);
				
			SetObstaclesMaterial (item);
			
			float pipeRotation =
				(start + i * direction) *
				360f / tunnel.pipeSegmentCount;

			rotations.Add (pipeRotation);

			item.Place (tunnel, i * angleStep, pipeRotation);
		}

		return rotations;
	}
}
