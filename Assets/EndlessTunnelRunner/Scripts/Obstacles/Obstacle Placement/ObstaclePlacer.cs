﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ObstaclePlacer : MonoBehaviour
{
	public Obstacle[] ObstaclePrefabs;
	public abstract List<float> GenerateObstacles (Tunnel tunnel);
	
	
	public void SetObstaclesMaterial (Obstacle obstacle)
	{
		obstacle.meshRenderer.material = LevelMaterial.instance.CurrentObstacleMaterial;
	}
}
