using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleManager : MonoBehaviour
{

	public ObstaclePlacer[] obstaclePlacers;
	public float[] ObstacleWeights;
	public bool[] WeightsEffectedByAudio;
	public float DefaultWeight = 1f;
	
	private float totalWeight;

	private List<int> volumeEffectsWeightIndexes = new List<int> ();
	private float[] InitialWeights;
	
	void Awake ()
	{
		if (ObstacleWeights.Length < obstaclePlacers.Length) {
			float[] weights = new float[obstaclePlacers.Length];
			
			for (int i = 0; i < ObstacleWeights.Length; ++i) {
				weights [i] = ObstacleWeights [i];
			}
			
			for (int i = ObstacleWeights.Length; i < obstaclePlacers.Length; ++i) {
				weights [i] = DefaultWeight;
			}
			
			ObstacleWeights = weights;
			
		} else if (ObstacleWeights.Length > obstaclePlacers.Length) {
			float[] weights = new float[obstaclePlacers.Length];
			
			for (int i = 0; i < obstaclePlacers.Length; ++i) {
				weights [i] = ObstacleWeights [i];
			}
			
			ObstacleWeights = weights;
			
		}

		InitialWeights = new float[ObstacleWeights.Length];

		for (int i = 0; i < ObstacleWeights.Length; i++) {
			if (WeightsEffectedByAudio.Length > i && WeightsEffectedByAudio [i]) {
				volumeEffectsWeightIndexes.Add (i);
				InitialWeights [i] = ObstacleWeights [i];
			}

			totalWeight += ObstacleWeights [i];
		}

	}

	private void RecalculateTotalWeight ()
	{
		totalWeight = 0f;
		foreach (var weight in ObstacleWeights) {
			totalWeight += weight;
		}
	}
	
	void Update ()
	{
		if (volumeEffectsWeightIndexes.Count == 0)
			return;

		foreach (var i in volumeEffectsWeightIndexes) {
			ObstacleWeights [i] = InitialWeights [i] + AudioPlayer.instance.GetVolume () * 1.5f;
		}

		RecalculateTotalWeight ();
	}
	
	
	public List<float> Generate (Tunnel tunnel)
	{
		RemoveCurrentObstacles ();

		var index = GetIndex ();
		
		return obstaclePlacers [index].GenerateObstacles (tunnel);
	}
	
	public void RemoveCurrentObstacles ()
	{
		for (int i = 0; i < transform.childCount; i++) {
			var child = transform.GetChild (i).gameObject;
			
			if (child.CompareTag ("Obstacle"))
				Destroy (child);
		}
	}
	
	private int GetIndex ()
	{
		var randomIndex = -1;
		var random = Random.value * totalWeight;
		
		for (int i = 0; i < ObstacleWeights.Length; ++i) {
			random -= ObstacleWeights [i];
			
			if (random <= 0f) {
				randomIndex = i;
				break;
			}
		}
		
		return randomIndex;
	}
}
