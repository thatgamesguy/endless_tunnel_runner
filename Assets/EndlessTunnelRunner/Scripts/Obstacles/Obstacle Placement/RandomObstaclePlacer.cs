﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomObstaclePlacer : ObstaclePlacer
{
	public int MinObstaclesSpawned = 1;
	public int MaxObstaclesSpawned = 10;
	
	void Awake ()
	{
		if (MinObstaclesSpawned > MaxObstaclesSpawned)
			MinObstaclesSpawned = MaxObstaclesSpawned;
		
	}
	
	public override List<float> GenerateObstacles (Tunnel tunnel)
	{
		var rotations = new List<float> ();

		var max = (MaxObstaclesSpawned > tunnel.CurveSegmentCount) ? tunnel.CurveSegmentCount : MaxObstaclesSpawned;
	
		var numOfObstacles = Random.Range (MinObstaclesSpawned, max);

		float angleStep = tunnel.CurveAngle / tunnel.CurveSegmentCount;
		for (int i = 0; i <= numOfObstacles; i++) {
			
			/*	if (Random.value < AudioPlayer.instance.GetVolume ()) {
				continue;
			}*/
		
			var item = Instantiate<Obstacle> (
				ObstaclePrefabs [Random.Range (0, ObstaclePrefabs.Length)]);
			
			SetObstaclesMaterial (item);
				
			float pipeRotation =
				(Random.Range (0, tunnel.pipeSegmentCount) + 0.5f) *
				360f / tunnel.pipeSegmentCount;

			rotations.Add (pipeRotation);

			item.Place (tunnel, i * angleStep, pipeRotation);
		}

		return rotations;
	}
	
}
