﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour
{
	public MeshRenderer meshRenderer { get; set; }
	protected Transform rotater;
	protected Transform obstacle;

	protected Vector3 initialScale;
	
	public virtual void Awake ()
	{
		rotater = transform.GetChild (0);
		obstacle = transform.GetChild (0).GetChild (0);
		initialScale = obstacle.localScale;
		
		meshRenderer = obstacle.GetComponent<MeshRenderer> ();
	}
	
	public void Place (Tunnel tunnel, float curveRotation, float ringRotation)
	{
		transform.SetParent (tunnel.transform, false);
		transform.localRotation = Quaternion.Euler (0f, 0f, -curveRotation);
		rotater.localPosition = new Vector3 (0f, tunnel.CurveRadius);
		rotater.localRotation = Quaternion.Euler (ringRotation, 0f, 0f);

	}

	protected void UpdateScale ()
	{
		var volume = AudioPlayer.instance.GetVolume () * 0.6f;

		obstacle.localScale = new Vector3 (initialScale.x + volume, initialScale.y + volume, initialScale.z + volume);
	}
	

	public virtual void Update ()
	{
		UpdateScale ();
	}

}
