﻿using UnityEngine;
using System.Collections;

public class PulseColourWithAudio : MonoBehaviour
{
	public Color MaxColour;
	public float RedOffset;
	public float BlueOffset;
	public float GreenOffset;
	public float AlphaOffset = 100f;
	
	private Renderer _renderer;
	
	// Use this for initialization
	void Awake ()
	{
		_renderer = GetComponent<Renderer> ();
		
		RedOffset = NormaliseOffset (RedOffset);
		BlueOffset = NormaliseOffset (BlueOffset);
		GreenOffset = NormaliseOffset (GreenOffset);
		AlphaOffset = NormaliseOffset (AlphaOffset);
		
	}
	
	private float NormaliseOffset (float offset)
	{
		if (offset > 1) {
			return offset / 255f;
		}
		
		return offset;
	}
	
	//rgb(236, 240, 241)
	
	// Update is called once per frame
	void Update ()
	{
		var vol = AudioPlayer.instance.GetVolume ();
	
		var red = Mathf.Min (vol + RedOffset, MaxColour.r);
		var blue = Mathf.Min (vol + BlueOffset, MaxColour.b);
		var green = Mathf.Min (vol + GreenOffset, MaxColour.g);
		var alpha = Mathf.Min (vol + AlphaOffset, MaxColour.a);
		
		_renderer.material.color = new Color (red, green, blue, alpha);
	}
}
