using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	public TunnelSystem pipeSystem;
	
	public float initialVelocity;
	public float rotationVelocity;
	public float acceleration = 0.05f;
	
	public HUD hud;
	
	private Tunnel currentPipe;
	
	private float distanceTraveled;
	
	private float deltaToRotation;
	private float systemRotation;
	
	private Transform world, rotater;
	
	private float worldRotation, avatarRotation;
	private float currentVelocity;
	
	private LevelSelectMenu menu;
	
	void Awake ()
	{
		world = pipeSystem.transform.parent;
		rotater = transform.GetChild (0);
		gameObject.SetActive (false);
	}
	
	void Start ()
	{

		deltaToRotation = 360f / (2f * Mathf.PI * currentPipe.CurveRadius);
	}
	

	void Update ()
	{
		currentVelocity += acceleration * Time.deltaTime * AudioPlayer.instance.GetVolume ();
	
		float delta = currentVelocity * Time.deltaTime;
		distanceTraveled += delta;

		UpdateSystemRotation (delta);
			
		UpdateAvatarRotation ();

	}
	
	
	public void StartGame ()
	{
		currentVelocity = initialVelocity;
		distanceTraveled = 0f;
		avatarRotation = 0f;
		systemRotation = 0f;
		worldRotation = 0f;
		
	
		currentPipe = pipeSystem.SetupFirstPipe ();
		SetupCurrentPipe ();

		
		gameObject.SetActive (true);
	}
	
	public void SetMenu (LevelSelectMenu menu)
	{
		this.menu = menu;
	}
	
	public void EndGame (bool complete)
	{
		menu.EndGame (complete);
		gameObject.SetActive (false);
	}
	
	private void UpdateSystemRotation (float delta)
	{
		systemRotation += delta * deltaToRotation;
		
		if (systemRotation >= currentPipe.CurveAngle) {
			delta = (systemRotation - currentPipe.CurveAngle) / deltaToRotation;
			currentPipe = pipeSystem.SetupNextPipe ();
			deltaToRotation = 360f / (2f * Mathf.PI * currentPipe.CurveRadius);
			SetupCurrentPipe ();
			systemRotation = delta * deltaToRotation;
		}
		
		
		pipeSystem.transform.localRotation =
			Quaternion.Euler (0f, 0f, systemRotation);
	}
	
	private void SetupCurrentPipe ()
	{
		deltaToRotation = 360f / (2f * Mathf.PI * currentPipe.CurveRadius);
		worldRotation += currentPipe.RelativeRotation;
		if (worldRotation < 0f) {
			worldRotation += 360f;
		} else if (worldRotation >= 360f) {
			worldRotation -= 360f;
		}
		world.localRotation = Quaternion.Euler (worldRotation, 0f, 0f);
	}
	
	private void UpdateAvatarRotation ()
	{
		float rotationInput = 0f;
		if (Application.isMobilePlatform) {
			if (Input.touchCount == 1) {
				if (Input.GetTouch (0).position.x < Screen.width * 0.5f) {
					rotationInput = -1f;
				} else {
					rotationInput = 1f;
				}
			}
		} else {
			rotationInput = Input.GetAxis ("Horizontal");
		}
		
		avatarRotation += rotationVelocity * Time.deltaTime * rotationInput;

		if (avatarRotation < 0f) {
			avatarRotation += 360f;
		} else if (avatarRotation >= 360f) {
			avatarRotation -= 360f;
		}

		rotater.localRotation = Quaternion.Euler (avatarRotation, 0f, 0f);
	}

}
