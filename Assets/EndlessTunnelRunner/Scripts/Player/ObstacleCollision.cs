﻿using UnityEngine;
using System.Collections;

public class ObstacleCollision : MonoBehaviour
{
	public ParticleSystem shape, trail, burst;
	
	public float deathCountdown = -1f;
	
	private Player player;
	
	void Awake ()
	{
		player = transform.root.GetComponent<Player> ();
	}
	
	void Update ()
	{
		if (deathCountdown >= 0f) {
			deathCountdown -= Time.deltaTime;
			if (deathCountdown <= 0f) {
				deathCountdown = -1f;
				shape.enableEmission = true;
				trail.enableEmission = true;
				player.EndGame (false);
			}
		}
	}
	
	void OnTriggerEnter (Collider collider)
	{
		if (collider.CompareTag ("Obstacle") && deathCountdown < 0f) {
			shape.enableEmission = false;
			trail.enableEmission = false;
			burst.Emit (burst.maxParticles);
			deathCountdown = burst.startLifetime;
		}
	}

}
