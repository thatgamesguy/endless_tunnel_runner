﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectibleManager : MonoBehaviour
{

	public CollectiblePlacer CollectiblePlacer;
	
	public void Generate (Tunnel tunnel, List<float> usedRotations)
	{
		RemoveCurrentCollectibles ();
		
		
		CollectiblePlacer.GenerateCollectibles (tunnel, usedRotations);
	}
	
	public void RemoveCurrentCollectibles ()
	{
		for (int i = 0; i < transform.childCount; i++) {
			var child = transform.GetChild (i).gameObject;
			
			if (child.CompareTag ("Collectible"))
				Destroy (child);
		}
	}
}
