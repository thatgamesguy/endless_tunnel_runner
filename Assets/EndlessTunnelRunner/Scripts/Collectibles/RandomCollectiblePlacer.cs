﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomCollectiblePlacer : CollectiblePlacer
{
	public override void GenerateCollectibles (Tunnel tunnel, List<float> usedRotations)
	{
		var numOfObstacles = Random.Range (1, tunnel.CurveSegmentCount);
		
		float angleStep = tunnel.CurveAngle / tunnel.CurveSegmentCount;
		for (int i = 0; i <= numOfObstacles; i++) {
			var item = Instantiate<Collectible> (
				CollectiblePrefabs [Random.Range (0, CollectiblePrefabs.Length)]);

			float pipeRotation;

			do {
				pipeRotation =
				(Random.Range (0, tunnel.pipeSegmentCount) + 0.5f) *
					360f / tunnel.pipeSegmentCount;
			} while (!usedRotations.Contains (pipeRotation));


			item.Place (tunnel, i * angleStep, pipeRotation);
		}
	}
}
