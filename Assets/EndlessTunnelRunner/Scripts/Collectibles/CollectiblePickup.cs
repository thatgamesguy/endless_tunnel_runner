﻿using UnityEngine;
using System.Collections;

public class CollectiblePickup : MonoBehaviour
{
	public GameObject Base;

	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Player")) {
			//	HighScore.instance.IncreaseMultiplier ();
			//	HUD.instance.UpdateMultiplierLabel ();
			Destroy (Base);
		} 
	}
}
