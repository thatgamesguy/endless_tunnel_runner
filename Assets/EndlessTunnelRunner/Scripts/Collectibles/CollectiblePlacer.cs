﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class CollectiblePlacer : MonoBehaviour
{
	public Collectible[] CollectiblePrefabs;
	public abstract void GenerateCollectibles (Tunnel tunnel, List<float> usedRotations);
}
