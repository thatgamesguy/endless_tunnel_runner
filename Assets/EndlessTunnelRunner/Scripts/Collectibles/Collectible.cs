﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour
{
	public float RotationVelocity = 120f;
	public float SeekDistance = 0.5f;
	public float SeekAngle;
	private Transform rotater;

	private float avatarRotation;

	private Transform _transform;

	public Transform world;
	public Transform parent;
	public Transform tunnel;

	public virtual void Awake ()
	{
		rotater = transform.GetChild (0);	

	}

/*	void Start ()
	{
		_transform = transform.parent;
		world = transform.root;
		parent = world.GetChild (0);
		tunnel = transform.parent;
	}*/

	/*void LateUpdate ()
	{

		if (!Vector3.Equals (_transform.localPosition, Vector3.zero))
			return;


		/*if (PlayerRotation.instance.PlayerOneRotation.HasValue) {
			if (Mathf.DeltaAngle (transform.localRotation.x, PlayerRotation.instance.PlayerOneRotation.Value.x) < 120f) {

				//var rot = rotater.localRotation;
				//rotater.localRotation = // Quaternion.Euler (PlayerRotation.instance.PlayerOneRotation.Value.x, 0f, 0f);
				//	new Quaternion (PlayerRotation.instance.PlayerOneRotation.Value.x - 60, rot.y, rot.z, rot.w);
				//rotater.localRotation = PlayerRotation.instance.PlayerOneRotation.Value;

				Debug.Log (Mathf.DeltaAngle (transform.localRotation.x, PlayerRotation.instance.PlayerOneRotation.Value.x));

				var rotationInput = 5f;

				avatarRotation += RotationVelocity * Time.deltaTime * rotationInput;
				
				if (avatarRotation < 0f) {
					avatarRotation += 360f;
				} else if (avatarRotation >= 360f) {
					avatarRotation -= 360f;
				}
				
				//rotater.localRotation = Quaternion.Euler (avatarRotation, 0f, 0f);
			}
		}

		if (PlayerRotation.instance.PlayerOneRotation.HasValue) {


			Vector3 vectorOne = PlayerRotation.instance.PlayerOneRotation.Value.eulerAngles;
			Vector3 vectorTwo = rotater.localRotation.eulerAngles;


			float diffX = Mathf.DeltaAngle (vectorOne.x, vectorTwo.x);
			//float diffY = Mathf.DeltaAngle (vectorOne.y, vectorTwo.y);
			//float diffZ = Mathf.DeltaAngle (vectorOne.z, vectorTwo.z);
		
			float difference = Mathf.Abs (diffX); // + Mathf.Abs (diffY) + Mathf.Abs (diffZ);

		
			if (difference < 60) {
	
				var s = transform.transform.localRotation.eulerAngles;

				var x = vectorOne.x - world.localRotation.x - parent.localRotation.x - tunnel.localRotation.x;

				if (x < 0f) {
					x += 360f;
				} else if (x >= 360f) {
					x -= 360f;
				}
			

				rotater.localRotation = Quaternion.Euler (x, vectorOne.y, vectorOne.z);
			} else {
				// Not aligned - Add other code here
			}
		}
	}*/
	
	public void Place (Tunnel tunnel, float curveRotation, float ringRotation)
	{
		transform.SetParent (tunnel.transform, false);
		transform.localRotation = Quaternion.Euler (0f, 0f, -curveRotation);
		rotater.localPosition = new Vector3 (0f, tunnel.CurveRadius);
		rotater.localRotation = Quaternion.Euler (ringRotation, 0f, 0f);
		
	}
}
