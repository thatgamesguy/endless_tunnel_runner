﻿using UnityEngine;
using System.Collections;

public class LevelMaterial : MonoBehaviour
{

	public Material CurrentPipeMaterial { get; set; }
	public Material CurrentObstacleMaterial { get; set; }
	
	private static LevelMaterial _instance;
	public static LevelMaterial instance { get { return _instance; } }
	
	void Awake ()
	{
		_instance = this;
	}
	

}
