﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class LevelCompletion : MonoBehaviour
{
	public int CurrentLevel { get; set; }
	
	public bool[] CompleteLight { get; set; }
	public bool[] CompleteDark { get; set; }
	
	private static LevelCompletion _instance;
	public static LevelCompletion instance { get { return _instance; } }
	
	private string filePathPost = "/leveldata.dat";

	void Awake ()
	{
		if (!_instance) {
			_instance = this;
			DontDestroyOnLoad (gameObject);
		} else if (_instance != this) {
			_instance = this;
		}
		
	}
	
	void OnDisable ()
	{
		Save ();
	}
	
	public void Load ()
	{
		
		if (File.Exists (Application.persistentDataPath + filePathPost)) {
			Debug.Log ("Loading Data");
			
			var bf = new BinaryFormatter ();
			var file = File.Open (Application.persistentDataPath + filePathPost, FileMode.Open);
			
			ScoreData data = (ScoreData)bf.Deserialize (file);
			
			file.Close ();
			
			this.CompleteLight = data.CompleteLight;
			this.CompleteDark = data.CompleteDark;
		}

		if (CompleteLight == null || CompleteLight.Length == 0) {
			CompleteLight = new bool[8];
		}

		if (CompleteDark == null || CompleteDark.Length == 0) {
			CompleteDark = new bool[8];
		}
	}
	
	public void Save ()
	{
		Debug.Log ("Saving Data");
		
		var bf = new BinaryFormatter ();
		var file = File.Create (Application.persistentDataPath + filePathPost);

		var data = new ScoreData (CompleteLight, CompleteDark);
		
		bf.Serialize (file, data);
		file.Close ();
	}



}

[Serializable]
class ScoreData
{
	private bool[] completeLight;
	public bool[] CompleteLight { get { return completeLight; } }

	private bool[] completeDark;
	public bool[] CompleteDark { get { return completeDark; } }
	
	public ScoreData (bool[] completeLight, bool[] completeDark)
	{
		this.completeLight = completeLight;
		this.completeDark = completeDark;
	}
}
